var mongojs = require('mongojs');
var booklist = mongojs('booklist', ['booklist']);
var users = mongojs('users', ['users']);

function creatDb() {
    users.users.find().toArray(function (err,docs) {
        if(err) throw err;
           if(docs.length==0){
               insertUsers()
           }else{
               process.exit();
           }
    });
}
function insertUsers() {
    users.users.insert([{login:'admin',password:'admin',email:'roman.bv20@gmail.com'},
        {login:'test',password:'test',email:'test@gmail.com'}], function (err,docs) {
            if(err) throw err;
            insertBooks();
        })
}
function insertBooks() {
    booklist.booklist.insert([{id:1,author:'Roman',title:'1996',book_rating:1,status:1,description:'this is first book from Roman Boyko.',image_src:'../images/book1.png'},
        {id:2,author:'Inna R',title:'1888',book_rating:3,status:1,description:'this is second book from Inna R.',image_src:'../images/book2.jpg'},
        {id:3,author:'Oleh',title:'1998',book_rating:4,status:0,description:'this is third book from Oleh.',image_src:''}],function (err,docs) {
            if(err) throw err;
            process.exit();
        })
}
creatDb();