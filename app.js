var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mongojs = require('mongojs');
var session = require('express-session');
var url = require.url;
var booklist = mongojs('booklist', ['booklist']);
var user = mongojs('users', ['users']);

var index = require('./controllers/index');
var users = require('./controllers/users');
var books = require('./controllers/books');
var add = require('./controllers/add');
var edit = require('./controllers/edit');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views/pages'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({secret:'ssshhhhh1',saveUninitialized: true,resave: true}));

app.use('/', index);
app.use('/users', users);
app.use('/books',books);
app.use('/add',add);
app.use('/edit',edit);

app.post('/login',function (req,res) {
  user.users.findOne({login:req.body.login,password:req.body.password},function (err,docs) {
    if(err) throw err;
    if(docs){
      req.session.user=docs.email;
      res.send('ok');
    }else{
      res.send('wrongLogOrPass');
    }
  })
});

app.get('/selectUsers',function (req,res) {
  user.users.find().toArray(function (err,docs) {
    if(err) throw err;
    res.send(docs);
  });
});

app.get('/booklist',function (req,res) {

  booklist.booklist.find().toArray(function (err,docs) {
    if (err) throw err;
    res.send(docs);
  });
});

app.post('/showbookinfo', function (req,res) {
  var bookInfo='';
  var bookRating='';
  booklist.booklist.find({id: Number(req.body.id)},function (err,docs) {
    if(err) throw err;
    for(doc in docs){
      if(docs[doc].book_rating==1){
        bookRating='<span>&#9733&#9734&#9734&#9734&#9734</span><br>';
      }else if(docs[doc].book_rating==2){
        bookRating='<span>&#9733&#9733&#9734&#9734&#9734</span><br>';
      }else if(docs[doc].book_rating==3) {
        bookRating = '<span>&#9733&#9733&#9733&#9734&#9734</span><br>';
      }else if(docs[doc].book_rating==4) {
        bookRating = '<span>&#9733&#9733&#9733&#9733&#9734</span><br>';
      }else if(docs[doc].book_rating==5) {
        bookRating = '<span>&#9733&#9733&#9733&#9733&#9733</span><br>';
      }else if(docs[doc].book_rating==0){
        bookRating = '<span>&#9734&#9734&#9734&#9734&#9734</span><br>';
      }
      bookInfo+='<img src="'+docs[doc].image_src+'"><h1>'+docs[doc].author+'</h1><br>'+
              bookRating+
          '<span>date</span><br>'+
          '<span>'+docs[doc].description+'</span><br>';
    }
    res.send(bookInfo);
  })
});

app.post('/addBook',function (req,res) {
  console.log(req.body);
  booklist.booklist.find().toArray(function (err,docs) {
    if (err) throw err;
    req.body.id=docs.length+1;
    req.body.id.toString();
    booklist.booklist.insert(req.body, function (err,doc) {
      res.send(doc);
    })
  });
});

app.get('/showBookToEdit',function (req,res) {
  var y = req.url.split('?');
  booklist.booklist.find({id:Number(y[1])}).toArray(function (err,docs) {
    res.send(docs);
  });
});


app.post('/editBook',function (req,res) {
  for(par in req.body) {
    booklist.booklist.findAndModify({
      query: {_id: mongojs.ObjectId(req.body[par]._id)},
      update: {
        $set: {
          author: req.body[par].author,
          title: req.body[par].title,
          status: req.body[par].status,
          description: req.body[par].description
        }
      }, new: true
    }, function (err, doc) {
      if (err) throw err;
      res.send('allOk');
  });
  }
});

app.post('/deleteBook',function (req,res) {
  booklist.booklist.remove({id:Number(req.body.id)},function (err,docs) {
    if(err) throw err;
    res.send('allOk');
  })
});
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
