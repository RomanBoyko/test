TO RUN SERVER LOCALLY YOU NEED:

0. Define node.js version number. ( in command line: node -v )
1. install NODE.JS version >= 4.4.7 on your machine (https://nodejs.org/en/download/)
2. go to root directory (where the 'package.json' file located)
3. run command "npm install" to install all dependencies
4. create file "config.js" in your root folder, and copy conents of the file "config.example" in your file "config.js"
5. open 2 cmd and run to your directory mongodb/server/run , start in first cmd "mongod", in second "mongo"
6. run command "npm run start" to run server on localhost:3000 (see "config to change port number ")
7. then you can login as 'test' 'test' or 'admin' 'admin'

HAVE FUN! :)
