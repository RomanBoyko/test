function login(form) {
    if (document.getElementById('email').value == "" ){
        alert("You must select a user");
        return;
    }
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            if(xhr.responseText=='ok') {
                window.location.href='/books';
            }else{
                document.getElementById('takeAnotherLogin').innerHTML = "Wrong login or password";
            }
        }else if (xhr.readyState == 4 && xhr.status != 200) {

        }
    };
    xhr.open('POST', '/login', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    var userInfo = 'login=' + document.getElementById('email').value+
        '&password='+document.getElementById('password').value;
    xhr.send(userInfo);
    return false;
}
