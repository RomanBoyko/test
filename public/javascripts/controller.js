var mainApp = angular.module("mainApp", []);

mainApp.controller('search',function ($scope) {
    $scope.search=function () {
        document.getElementById('search').value='';
    };
    $scope.clear=function () {
        document.getElementById('search').value='';
    };
});
function func(all) {
    console.log('11');
    var nowMark = Number($scope.getAttribute("mark"));
    //Show Now Mark
    alert(nowMark);
}
// function func(all) {
//     console.log('11');
//     var nowMark = Number($scope.getAttribute("mark"));
//     //Show Now Mark
//     alert(nowMark);
// }
mainApp.controller('showBooks', function($scope,$http) {
    $http.get('/booklist').then(function (response) {
        $scope.booklist = response.data;
    })
        .catch(function(response) {
        console.error('Gists error', response.status, response.data);
    })
        .finally(function() {
        });
    $scope.removeBook=function (id) {
        var obj=new Object();
        obj.id=id;
        $http.post('/deleteBook',obj).then(function () {
            window.location.reload();
        });
    };
    $scope.redirectToEdit=function (id) {
        window.location.href=('/edit?id='+id);
    };
    var el = document.getElementsByClassName("MJsKey");
    for (var i = 0; i < el.length; i++) {
        elem = el[i];
        if (Number(elem.getAttribute("status"))==1) {
            elem.innerHTML = "ON";

        }
        else if (Number(elem.getAttribute("status"))==0) {
            elem.innerHTML = "OFF";
        }
        $scope.changeStatus = function(){
            console.log($scope);
            if (Number($scope.getAttribute("status"))==1) {
                $scope.innerHTML = "OFF";
                $scope.setAttribute("status","0");
                $scope.setAttribute("class","MJsKeyOff");
            }
            else if (Number($scope.getAttribute("status"))==0) {
                $scope.innerHTML = "ON";
                $scope.setAttribute("status","1");
                $scope.setAttribute("class","MJsKeyOn");
            }
        }
    }

    // $scope.rating=function () {
    //     console.log($scope);
    //     var nowMark = Number($scope.getAttribute("mark"));
    //     //Show Now Mark
    //     alert(nowMark);
    // }
});

mainApp.controller('selectUsers', function($scope,$http) {
    $http.get('/selectUsers').then(function (response) {
        $scope.user = response.data;
    })
        .catch(function(response) {
            console.error('Gists error', response.status, response.data);
        })
        .finally(function() {

        });

});

mainApp.controller('addBook', function($scope,$http) {
    $scope.addBook = function() {
       $scope.book.book_rating='';
       $scope.book.image_src='';
       $http.post('/addBook',$scope.book).then(function (response) {
           window.location.href='/books';
       });
     }
});

mainApp.controller('editBook', function($scope,$http) {
    var y = window.location.search.split('=');
    $http.get('/showBookToEdit?'+y[1]).then(function (response) {
        $scope.bookedit = response.data;
    });
    $scope.editBook = function() {
        console.log($scope.bookedit);
        $http.post('/editBook',$scope.bookedit).then(function () {
            window.location.href='/books';
        });
    }
});