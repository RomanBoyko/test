function showBookInfo(e) {
    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function () {
        if (xhr.readyState == 4 && xhr.status == 200) {
            document.getElementById('bookinfo').innerHTML=xhr.responseText;
        }else if (xhr.readyState == 4 && xhr.status != 200) {
            document.getElementById('bookinfo').innerHTML=xhr.responseText;
        }
    };
    xhr.open('POST', '/showbookinfo', true);
    xhr.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
    var index = 'id=' + e.getAttribute('name');
    xhr.send(index);
    return false;
}
